import React, { useEffect } from "react";

const tasksContext = React.createContext(null);

export const TasksProvider = ({children}) => {
  const [tasksValue, setTasksValue] = React.useState([]);

  useEffect(() => {
    fetch('/api/get_tasks')
      .then(res => res.json())
      .then(res => {
        setTasksValue(res.tasks);
      });
  }, []);

  return  <tasksContext.Provider value={{ tasksValue, update: setTasksValue }}>
  {children}
  </tasksContext.Provider>;
}

export const useTasks = () => {
  const { tasksValue, update } = React.useContext(tasksContext);
  return { tasksValue, update }
}