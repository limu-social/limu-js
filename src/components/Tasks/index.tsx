import React from "react";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Task from "./Task";
import Box from '@mui/material/Box';
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { Checkbox } from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import NewTaskModal from "./NewTaskModal";
import { v4 as uuidv4 } from 'uuid';
import { useTasks } from "src/context/tasks";

interface TasksProps{
  done: Boolean;
}

export default function Tasks({done} : TasksProps){
  const { tasksValue, update }  = useTasks();
  const [open, setOpen] = React.useState(false);

  const addTask = (task) => {
    const fullTask = {
      ...task,
      id: uuidv4()
    }
    update([...tasksValue, fullTask]);
  };

  const updateTask = (task) => {
    const index = tasksValue.findIndex(x => x.id === task.id);
    update([...tasksValue.slice(0, index), task, ...tasksValue.slice(index+1, tasksValue.length)]);
  };

  return <List>
      <ThemeProvider
        theme={createTheme({
          components: {
            MuiListItemButton: {
              defaultProps: {
                disableTouchRipple: true,
              },
            },
          },
          palette: {
            mode: 'light',
            primary: { main: 'rgb(102, 157, 246)' },
            background: { paper: 'rgb(255, 255, 255)' },
          },
        })}
      >
        <ListItem sx={{
          width: 1,
          px: 3,
          pt: 2.5,
        }}>
          <NewTaskModal open={open} setOpen={(val) => setOpen(val)} addTask={addTask}/> 
          <AddIcon onClick={(e) => {e.stopPropagation(); setOpen(true);}}/>
        </ListItem>
        {tasksValue.map((task) => 
          task.done === done && <Task {...task} key={task.id} setDone={(value) => updateTask({...task, done: value})}/>
        )}
      </ThemeProvider>
  </List>
}