import React from 'react';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { FormControl } from '@mui/material';
import MDEditor from '@uiw/react-md-editor';
import Button from '@mui/material/Button';
import { useTasks } from "src/context/tasks";

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function NewTaskModal(props){
  const { tasksValue, update }  = useTasks();
  const [value, setValue] = React.useState(props.id? tasksValue.find(x => x.id === props.id).description : "**Task Description**");  
  const [label, setLabel] = React.useState(props.id? tasksValue.find(x => x.id === props.id).label : "");
  const [summary, setSummary] = React.useState(props.id? tasksValue.find(x => x.id === props.id).summary : "");

  const submitForm = (e) => {
    e.preventDefault();
    props.setOpen(false);

    const task = {
      label: e.target[0].value,
      summary: e.target[1].value,
      description: e.target[19].value,
      done: false
    };

    if(props.id){
      const index = tasksValue.findIndex(x => x.id === props.id);
      const currTask = {
        ...tasksValue[index],
        ...task
      }
      update([...tasksValue.slice(0, index), currTask, ...tasksValue.slice(index+1, tasksValue.length)]);
      return;
    }

    props.addTask(task);
  }

  return <Modal
    open={props.open}
    onClose={() => props.setOpen(false)}
    aria-labelledby="modal-modal-title"
    aria-describedby="modal-modal-description"
  >
    <Box sx={style}>
      <Typography id="modal-modal-title" variant="h6" component="h2">
        {props.id? "Update task" : "Add a new task"}
      </Typography>
      <form onSubmit={submitForm}>
        <TextField id="standard-basic" onChange={(e) => setLabel(e.target.value)}  value={label} label="Task label" variant="standard" sx={{ pb: 1}} fullWidth/>
        <TextField id="standard-basic" onChange={(e) => setSummary(e.target.value)} value={summary} label="Task Summary" variant="standard" sx={{ pb: 3}} fullWidth/>
        <MDEditor
          value={value}
          onChange={setValue}
        />
        <Box paddingTop={3} display="flex" flexDirection="row">
          <Box paddingRight={3}>
            <Button type="submit" variant="contained">{props.id? "Update" : "Add"}</Button>
          </Box>
          <Button variant="outlined" onClick={() => props.setOpen(false)}>Cancel</Button>
        </Box>
      </form>
    </Box>
  </Modal>;
}