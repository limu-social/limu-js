import React from "react";
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import KeyboardArrowDown from '@mui/icons-material/KeyboardArrowDown';
import Box from '@mui/material/Box';
import { Button, Checkbox, Typography } from "@mui/material";
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import EditIcon from '@mui/icons-material/Edit';
import NewTaskModal from "src/components/Tasks/NewTaskModal";

interface TaskProps{
  id: string;
  label: string;
  summary?: string;
  description?: string;
  done: boolean;
  setDone: (value: boolean) => void;
}

type ContentProps = {
  open: Boolean,
  summary?: string,
  description?: string
};

function Content({...props}: ContentProps){
  return <>
    { props.open && 
        <ReactMarkdown children={props.description} remarkPlugins={[remarkGfm]}/>
    }
    { !props.open && props.summary }
  </>
}

export default function Task({
  id,
  label,
  summary,
  description,
  done,
  setDone
}: TaskProps){
  const [open, setOpen] = React.useState(false);
  const [editOpen, setEditOpen] = React.useState(false);

  const secondaryTypographyProps : Object = open? {
    noWrap: true,
    fontSize: 12,
    lineHeight: '16px',
    component: 'span'
  } : { component: 'span' };

  const onChange = (e: React.ChangeEvent<HTMLButtonElement>) => {
    setDone(!done);
  };

  return <ListItem>
    <NewTaskModal open={editOpen} setOpen={(val) => setEditOpen(val)} id={id}/> 
    <Box
      sx={{
        pb: open ? 2 : 0,
        width: 1,
      }}
    >
      <ListItemButton
        alignItems="flex-start"
        onClick={() => setOpen(!open)}
        
        sx={{
          width: 1,
          px: 3,
          pt: 2.5,
        }}
      >
        <ListItemText
          primary={label}
          primaryTypographyProps={{
            fontSize: 20,
            fontWeight: 'medium',
            lineHeight: '20px',
            mb: '2px',
          }}
          secondary={<Content open={open} summary={summary} description={description}/>}
          secondaryTypographyProps={secondaryTypographyProps}
          sx={{ my: 0 }}
        />
        <EditIcon onClick={(e) => { e.stopPropagation(); setEditOpen(true);}}/>
        <Checkbox checked={done} onChange={onChange} onClick={(e) => e.stopPropagation()} sx={{ paddingTop: 0}}/>
        <KeyboardArrowDown
          sx={{
            mr: -1,
            transform: open ? 'rotate(-180deg)' : 'rotate(0)',
            transition: '0.2s',
          }}
        />
      </ListItemButton>
    </Box>
  </ListItem>
}