import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import Register from './pages/Register';
import Reset from './pages/Reset';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

const App = () => {
    return(
        <BrowserRouter>
            <Routes>
                <Route path='/limu/' element={<Dashboard/>}/>
                <Route path='/limu/login' element={<Login/>}/>
                <Route path='/limu/register' element={<Register/>} />
                <Route path='/limu/reset' element={<Reset/>} />
            </Routes>
        </BrowserRouter>
    )
}

export default App;

