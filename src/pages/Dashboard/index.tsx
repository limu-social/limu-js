import React from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import PrimarySearchAppBar from "src/components/PrimaryAppBar";
import './styles.css';
import AppTabs from "src/components/AppTabs";
import { Box } from "@mui/material";
import { TasksProvider } from 'src/context/tasks';
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate } from "react-router-dom";
import { auth, db } from "src/firebase";
import { query, collection, getDocs, where } from "firebase/firestore";

export default function Dashboard(){
  const [user, loading, error] = useAuthState(auth);
  const [name, setName] = React.useState("");
  const navigate = useNavigate();
  const fetchUserName = async () => {
    try {
      const q = query(collection(db, "users"), where("uid", "==", user?.uid));
      const doc = await getDocs(q);
      const data = doc.docs[0].data();
      setName(data.name);
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching user data");
    }
  };
  React.useEffect(() => {
    if (loading) return;
    if (!user) return navigate("/limu/login");
    fetchUserName();
  }, [user, loading]);

  return <Box>
      <PrimarySearchAppBar/>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        minHeight="100vh"
      >
        <TasksProvider>
          <AppTabs/>
        </TasksProvider>
      </Box>
  </Box>
}