import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate } from "react-router-dom";
import { TextField, Button } from "@mui/material";
import {
  auth,
  registerWithEmailAndPassword,
  signInWithGoogle,
} from "src/firebase";

import "./styles.css";

function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [user, loading, error] = useAuthState(auth);
  const navigate = useNavigate();
  const register = () => {
    if (!name) alert("Please enter name");
    registerWithEmailAndPassword(name, email, password);
  };
  useEffect(() => {
    if (loading) return;
    if (user) navigate("/limu");
  }, [user, loading]);
  return (
    <div className="register">
      <div className="register__container">
        <TextField id="standard-basic" label="Name" value={name} onChange={(e) => setName(e.target.value)} variant="standard" sx={{ pb: 1}} fullWidth/>
        <TextField id="standard-basic" label="Email" value={email} onChange={(e) => setEmail(e.target.value)} variant="standard" sx={{ pb: 1}} fullWidth/>
        <TextField id="standard-basic" type="password" label="Password" value={password} onChange={(e) => setPassword(e.target.value)} variant="standard" sx={{ pb: 1}} fullWidth/>
        <Button className="register__btn" onClick={register} variant="contained">
          Register
        </Button>
        <Button
          className="register__btn register__google"
          onClick={signInWithGoogle}
        >
          Register with Google
        </Button>
        <div>
          Already have an account? <Link to="/limu/login">Login</Link> now.
        </div>
      </div>
    </div>
  );
}
export default Register;