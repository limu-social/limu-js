import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { auth, logInWithEmailAndPassword, signInWithGoogle } from "src/firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import TextField from '@mui/material/TextField';
import { Button } from "@mui/material";
import "./styles.css";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, loading, error] = useAuthState(auth);
  const navigate = useNavigate();
  useEffect(() => {
    if (loading) {
      // maybe trigger a loading screen
      return;
    }
    if (user) navigate("/limu");
  }, [user, loading]);
  return (
    <div className="login">
      <div className="login__container">
        <TextField id="standard-basic" label="Email" value={email} onChange={(e) => setEmail(e.target.value)} variant="standard" sx={{ pb: 1}} fullWidth/>
        <TextField id="standard-basic" type="password" label="Password" value={password} onChange={(e) => setPassword(e.target.value)} variant="standard" sx={{ pb: 1}} fullWidth/>
        <Button className="login__btn" onClick={() => logInWithEmailAndPassword(email, password)} variant="contained">
          Login
        </Button>
        <Button
          className="login__btn login__google"
          onClick={signInWithGoogle}
        >
          Login with Google
        </Button>
        <div>
          <Link to="/reset">Forgot Password</Link>
        </div>
        <div>
          Don't have an account? <Link to="/limu/register">Register</Link> now.
        </div>
      </div>
    </div>
  );
}
export default Login;