const router = require('express').Router();

router.get('/get_tasks', (req, res) => res.json({
  tasks: [
    {
      id: '0',
      label: 'Solve math problems',
      summary: 'I have a math analysis assignment',
      description: `
## Heading one
I Realy **have** to do math

![math](https://marvel-b1-cdn.bc0a.com/f00000000026007/resilienteducator.com/wp-content/uploads/2012/11/AdobeStock_60467600_cup.jpg)
      `,
      done: false, 
    },
    {
      id: '1',
      label: 'Wash dishes',
      summary: 'Must do or wife -> mad',
      description: `
Just a normal string
      ` ,
      done: false,
    },
    {
      id: '2',
      label: 'Wash clothes',
      summary: 'Must do or wife -> mad',
      description: `
Just a normal string
      ` ,
      done: true,
    },
  ]
}))

module.exports = router;
